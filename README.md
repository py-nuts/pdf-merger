# PDF Merger
A ferramenta PDF Merger é uma aplicação de linha de comando (CLI) desenvolvida em Python que permite unir vários arquivos PDF em um único arquivo PDF. Esta ferramenta é útil para simplificar o processo de combinar múltiplos documentos PDF em um único arquivo, facilitando a organização e compartilhamento de documentos.

## Instalação
Para instalar o PDF Merger, siga estas etapas:

Clone este repositório para o seu sistema local:

```bash
git clone https://github.com/seu-usuario/pdf-merger.git
```
Navegue até o diretório do projeto:

```bash
cd pdf-merger
```
Instale as dependências usando pip:
```
pip install -r requirements.txt
```
## Uso

A ferramenta PDF Merger oferece duas flags opcionais:

- `--output`: Especifica o diretório onde a ferramenta irá buscar os arquivos PDF para mesclar. Se não for especificado, a ferramenta irá usar o diretório atual.
- `--filename`: Permite especificar o nome do arquivo PDF de saída. Se não for especificado, o arquivo de saída será nomeado como `merged.pdf`.

Aqui está como você pode usar a ferramenta:


```css
python pdf_merger.py --output /caminho/do/seu/diretorio --filename arquivo_saida.pdf
```
Se você não especificar nenhuma das flags, a ferramenta usará os valores padrão.

## Exemplo
Aqui está um exemplo de como você pode usar a ferramenta para mesclar arquivos PDF:

```css
python pdf_merger.py --output /caminho/do/seu/diretorio --filename arquivo_saida.pdf
```
Este comando irá mesclar todos os arquivos PDF encontrados no diretório especificado e salvar o arquivo mesclado com o nome especificado.

## Contribuições
Contribuições são bem-vindas! Se você encontrar algum problema ou tiver sugestões de melhorias, sinta-se à vontade para abrir uma issue ou enviar um pull request neste repositório.

## Licença
Este projeto está licenciado sob a MIT License.

import pathlib
import PyPDF2
import argparse

parser = argparse.ArgumentParser(description='Simple PDF Merger CLI')
parser.add_argument('--output', dest='output', help='Caminho do diretório onde contém os arquivos .pdf')
parser.add_argument('--filename', dest='filename', help='Nomear arquivo mergeado')
args = parser.parse_args()

if args.output:
    ROOT_DIR = pathlib.Path(args.output)
else:
    ROOT_DIR = pathlib.Path(__file__).parent

merger = PyPDF2.PdfMerger()

files = list(ROOT_DIR.glob('*.pdf'))

if not files:
    print(f'Não foi encontrado nenhum arquivo .pdf no diretório {ROOT_DIR}.')
else:
    for file in files:
        print(file)
        merger.append(file)

    if args.filename:
        filename = f'{args.filename}.pdf'
    else:
        filename = 'merged.pdf'

    try:
        merger.write(ROOT_DIR/filename)
    except PermissionError:
        print(f"Você não tem permissão para salvar o arquivo aqui: {ROOT_DIR}.")